package com.sudoku;

import javax.swing.JFrame;

public class SudokuFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private Sudoku sudokuPanel;

	public SudokuFrame(String title) {
		super(title);
		sudokuPanel = new Sudoku();
		this.setContentPane(sudokuPanel);
		this.setSize(500, 660);
		this.setLocationRelativeTo(null);
	}

	public SudokuFrame(String title, int level) {
		super(title);
		sudokuPanel = new Sudoku(level);
		this.setContentPane(sudokuPanel);
		this.setSize(500, 660);
		this.setLocationRelativeTo(null);
	}
}
