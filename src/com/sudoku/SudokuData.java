package com.sudoku;

import java.util.ArrayList;
import java.util.Arrays;

public class SudokuData {
	public static final int PUZZLE_MAX_SIZE = 9;
	public static final int REGION_MAX_SIZE = 3;

	private ArrayList<SudokuData.Data> data = new ArrayList<SudokuData.Data>();

	public SudokuData(int[][] data) {
		validateData(data);
		for (int x = 0; x < PUZZLE_MAX_SIZE; x++) {
			for (int y = 0; y < PUZZLE_MAX_SIZE; y++) {
				SudokuData.Data d = new SudokuData.Data();
				d.val = data[x][y];
				d.x = x;
				d.y = y;
				d.boxNum = InputBox.getBoxNum(x, y);
			}
		}
	}

	public SudokuData(ArrayList<SudokuData.Data> data) {
		this.data = data;
	}

	public static class Data {
		int x;
		int y;
		int val;
		int boxNum;
	}

	private void validateData(int[][] data) throws RuntimeException {
		for (int[] rows : data) {
			if (rows.length != 9)
				throw new RuntimeException("not valid data row length not matched");
			for (int val : rows) {
				if (val < 1 || val > 9) {
					throw new RuntimeException("not valid data has to be between 1 and 9");
				}
			}
		}
	}

	public boolean checkData(ArrayList<Integer> data) {
		System.out.println(data);
		boolean[] check = new boolean[PUZZLE_MAX_SIZE];
		Arrays.fill(check, false);
		for (int item : data) {
			System.out.println(item);
			if (check[item - 1]) {
				System.out.println("found duplicate " + item);
				return false;
			} else {
				check[item - 1] = true;
			}
		}
		boolean res = true;
		for (boolean val : check) {
			res = res && val;
		}
		return res;
	}

	public boolean checkRow() {
		System.out.println("checkrow");
		boolean res = true;
		for (int rowId = 0; rowId < 9; rowId++) {
			ArrayList<Integer> row = new ArrayList<Integer>();
			for (Data d : data) {
				if (d.y == rowId)
					row.add(d.val);
			}
			res = res && checkData(row);
		}
		return res;
	}

	public boolean checkColumn() {
		System.out.println("checkcol");
		boolean res = true;
		for (int colId = 0; colId < 9; colId++) {
			ArrayList<Integer> col = new ArrayList<Integer>();
			for (Data d : data) {
				if (d.x == colId)
					col.add(d.val);
			}
			res = res && checkData(col);
		}
		return res;
	}

	public boolean checkBox() {
		System.out.println("checkbox");
		boolean res = true;
		for (int boxId = 0; boxId < 9; boxId++) {
			ArrayList<Integer> box = new ArrayList<Integer>();
			for (Data d : data) {
				if (d.boxNum == boxId)
					box.add(d.val);
			}
			res = res && checkData(box);
		}
		return res;
	}

	public boolean isSukoduSolved() {
		// System.out.println("isFilled: "+isFilled());
		// System.out.println("checkBox: "+checkBox());
		// System.out.println("checkColumn: "+checkColumn());
		// System.out.println("checkRow: "+checkRow());
		if (isFilled() && checkBox() && checkColumn() && checkRow())
			return true;
		return false;
	}

	public boolean isFilled() {
		for (Data d : data) {
			if (!(d.val >= 1 && d.val <= 9)) {
				return false;
			}
		}
		return true;
	}
}
