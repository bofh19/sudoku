package com.sudoku;

public class Main {

	public static void main(String[] args) {
		try {
			String arg = args[0];
			String[] parts = arg.split("=");
			String part = parts[1];
			System.out.println(part);
			if (part.equals("beginner")) {
				startLevel(1);
			} else if (part.equals("intermediate")) {
				startLevel(2);
			} else if (part.equals("expert")) {
				startLevel(3);
			} else {
				startSomeThing();
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			startSomeThing();
		}
		checkData();
	}

	private static void startSomeThing() {
		SudokuFrame frame = new SudokuFrame("Sudoku");
		frame.setVisible(true);
	}

	private static void startLevel(int level) {
		SudokuFrame frame = new SudokuFrame("Sudoku", level);
		frame.setVisible(true);
	}
	
	private static void checkData(){
		for(int[][] data:SudokuValues.beginner){
			if(data.length<9) System.out.println("fail "+data);
			int rc = 0;
			for(int[] row:data){
				rc++;
				if(row.length<9) System.out.println("fail row "+row+"/"+data[0][0]+"/"+data[0][1]+"/"+rc);
			}
		}
	}
	
	
}
