package com.sudoku;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Sudoku extends JPanel implements MouseMotionListener, MouseListener, KeyListener {

	int[][] initialPuzzle;

	private static final long serialVersionUID = 1L;
	private static Font serifFont = new Font("Serif", Font.BOLD, 30);
	private static Font serifFontSmall = new Font("Serif", Font.ITALIC, 14);
	Color hitColor = Color.green;
	Color missColor = Color.lightGray;
	int gridPadding = 25;
	int boxWidth = 50;
	int boxHeight = 50;
	int lineWidth = 3;
	private String level = "";
	private int levelInt;

	static class SuperRect {
		int x;
		int y;
		int width;
		int height;
	}

	SuperRect superRect;

	ArrayList<InputBox> gridBoxes = new ArrayList<InputBox>();

	public Sudoku() {
		this(1);
	}

	public Sudoku(int level) {
		this.levelInt = level;
		addMouseMotionListener(this);
		addMouseListener(this);
		addKeyListener(this);
		setFocusable(true);
		requestFocusInWindow();
		if (level == 1)
			this.level = "beginner";
		else if (level == 2)
			this.level = "intermediate";
		else if (level == 3)
			this.level = "expert";
		initialPuzzle = SudokuValues.getAGrid(level);

		generateInputBoxes();
	}

	private void generateInputBoxes() {
		gridBoxes.clear();
		for (int x = 0; x < 9; x++) {
			for (int y = 0; y < 9; y++) {
				InputBox bx = new InputBox();
				if (x >= 6)
					bx.x = 2 * lineWidth + gridPadding + x * boxWidth;
				else if (x >= 3)
					bx.x = lineWidth + gridPadding + x * boxWidth;
				else
					bx.x = gridPadding + x * boxWidth;
				if (y >= 6)
					bx.y = 2 * lineWidth + gridPadding + y * boxHeight;
				else if (y >= 3)
					bx.y = lineWidth + gridPadding + y * boxHeight;
				else
					bx.y = gridPadding + y * boxHeight;
				bx.matrixX = x;
				bx.matrixY = y;
				bx.width = boxWidth;
				bx.height = boxHeight;
				bx.editable = true;
				bx.val = initialPuzzle[y][x];
				if (bx.val != -1)
					bx.editable = false;
				System.out.println(bx);
				bx.boxNum = InputBox.getBoxNum(x, y);
				gridBoxes.add(bx);
			}
		}
		superRect = new SuperRect();
		InputBox firstBX = gridBoxes.get(0);
		InputBox lastBX = gridBoxes.get(gridBoxes.size() - 1);
		superRect.x = firstBX.x - lineWidth;
		superRect.y = firstBX.y - lineWidth;
		superRect.width = (lastBX.x + lastBX.width - firstBX.x + 2 * lineWidth);
		superRect.height = (lastBX.y + lastBX.height - firstBX.y + 2 * lineWidth);
	}

	@Override
	public void paintComponent(Graphics canvas) {
		canvas.setFont(serifFontSmall);
		canvas.drawString(level, 10, 14);
		canvas.setFont(serifFont);
		canvas.setColor(Color.blue);
		canvas.fillRect(superRect.x, superRect.y, superRect.width, superRect.height);
		for (InputBox bx : gridBoxes) {
			canvas.setColor(Color.white);
			canvas.fillRect(bx.x, bx.y, bx.width, bx.height);
			canvas.setColor(Color.black);
			canvas.drawRect(bx.x, bx.y, bx.width, bx.height);
			if (bx.val != -1) {
				if (bx.editable)
					canvas.setColor(Color.blue);
				else
					canvas.setColor(Color.black);
				canvas.drawString("" + bx.val, bx.x + 15, bx.y + 35);
			}
			// canvas.drawString("" + bx.matrixX+"/"+bx.matrixY, bx.x + 15, bx.y
			// + 35);
		}

		if (sBox != null) {
			canvas.setColor(hitColor);
			canvas.fillRect(sBox.x, sBox.y, sBox.width, sBox.height);
			canvas.setColor(Color.BLACK);
			canvas.drawRect(sBox.x, sBox.y, sBox.width, sBox.height);
			if (sBox.val != -1) {
				if (sBox.editable)
					canvas.setColor(Color.blue);
				else
					canvas.setColor(Color.black);
				canvas.drawString("" + sBox.val, sBox.x + 15, sBox.y + 35);
			}
		}

		if (hBox != null && (sBox == null || hBox.notEquals(sBox))) {
			canvas.setColor(Color.white);
			canvas.fillRect(hBox.x, hBox.y, hBox.width, hBox.height);
			canvas.setColor(hitColor);
			canvas.drawRect(hBox.x, hBox.y, hBox.width, hBox.height);
			if (hBox.val != -1) {
				if (hBox.editable)
					canvas.setColor(Color.blue);
				else
					canvas.setColor(Color.black);
				canvas.drawString("" + hBox.val, hBox.x + 15, hBox.y + 35);
			}
		}

		canvas.setColor(buttonColor);
		canvas.fillRect(buttonX, buttonY, buttonWidth, buttonHeight);
		canvas.setColor(buttonTextColor);
		canvas.drawString(buttonText, buttonX + 15, buttonY + 35);

		canvas.setColor(newButtonColor);
		canvas.fillRect(newButtonX, newButtonY, newButtonWidth, newButtonHeight);
		canvas.setColor(newButtonTextColor);
		canvas.drawString(newButtonText, newButtonX + 15, newButtonY + 35);

		if (writeText)
			canvas.drawString(text, textX, textY + 35);
	}

	int buttonX = 25;
	int buttonY = 500;
	int buttonWidth = 200;
	int buttonHeight = 50;
	Color buttonColor = Color.cyan;
	Color buttonTextColor = Color.magenta;
	String buttonText = "Check Status";
	Rectangle buttonRect = new Rectangle(buttonX, buttonY, buttonWidth, buttonHeight);

	int newButtonX = 25;
	int newButtonY = 500 + buttonHeight + 15;
	int newButtonWidth = 200;
	int newButtonHeight = 50;
	Color newButtonColor = Color.cyan;
	Color newButtonTextColor = Color.magenta;
	String newButtonText = "New";
	Rectangle newButtonRect = new Rectangle(newButtonX, newButtonY, newButtonWidth, newButtonHeight);

	int textX = buttonX + buttonWidth + 15;
	int textY = buttonY;
	boolean writeText = false;
	String text = "";
	Color textColor = buttonColor;

	@Override
	public void mouseDragged(MouseEvent e) {

	}

	InputBox sBox;
	InputBox hBox;

	@Override
	public void mouseMoved(MouseEvent e) {
		for (InputBox bx : gridBoxes) {
			Point mousePos = e.getPoint();
			boolean insideRect = isInsideRect(bx.getRect(), mousePos);
			if (bx.editable && insideRect && hBox == null) {
				hBox = bx;
				repaint();
				break;
			}
			if (bx.editable && insideRect && (hBox.matrixX != bx.matrixX || hBox.matrixY != bx.matrixY)) {
				hBox = bx;
				repaint();
				break;
			}
		}
	}

	public boolean isInsideRect(Rectangle rectangle, Point p) {
		return rectangle.contains(p);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (isInsideRect(buttonRect, e.getPoint())) {
			onCheckButtonClicked();
			return;
		} else if (isInsideRect(newButtonRect, e.getPoint())) {
			reloadWithNewData();
		}
		sBox = hBox;
		repaint();

	}

	private void reloadWithNewData() {
		writeText = false;
		initialPuzzle = SudokuValues.getAGrid(levelInt);
		generateInputBoxes();
		repaint();
	}

	private void onCheckButtonClicked() {
		ArrayList<SudokuData.Data> data = new ArrayList<SudokuData.Data>();
		for (InputBox bx : gridBoxes) {
			SudokuData.Data d = new SudokuData.Data();
			d.x = bx.matrixX;
			d.y = bx.matrixY;
			d.boxNum = bx.boxNum;
			d.val = bx.val;
			data.add(d);
		}
		SudokuData d = new SudokuData(data);
		writeText = true;
		if (d.isSukoduSolved())
			text = "Solved";
		else
			text = "Not yet solved";
		repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {

	}

	@Override
	public void mouseReleased(MouseEvent e) {

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {
		try {
			int i = Integer.parseInt(e.getKeyChar() + "");
			if (i >= 1 && i <= 9 && sBox != null && sBox.editable) {
				sBox.val = i;
				repaint();
			}
		} catch (Exception e2) {
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println(e.getKeyChar());
	}

	@Override
	public void keyReleased(KeyEvent e) {
		System.out.println(e.getKeyChar());
	}
}
