package com.sudoku;

import java.awt.Rectangle;

public class InputBox {
	int x;
	int y;
	int width;
	int height;
	int val = -1;

	int boxNum;

	@Override
	public boolean equals(Object obj) {
		InputBox b = (InputBox) obj;
		if (b.matrixX == matrixX && b.matrixY == matrixY)
			return true;
		return false;
	}

	public boolean notEquals(Object obj) {
		return !equals(obj);
	}

	boolean editable;

	int matrixX;
	int matrixY;

	@Override
	public String toString() {
		return matrixX + "/" + matrixY + "/" + "--" + (x) + "/" + (y);
	}

	private Rectangle rect;

	public Rectangle getRect() {
		if (rect == null)
			rect = new Rectangle(x, y, width, height);
		return rect;
	}

	public static int getBoxNum(int x, int y) {
		if (x < 3 && y < 3)
			return 0;
		else if (x < 3 && y >= 3 && y < 6)
			return 1;
		else if (x < 3 && y >= 6)
			return 2;

		else if (x >= 3 && x < 6 && y < 3)
			return 3;
		else if (x >= 3 && x < 6 && y >= 3 && y < 6)
			return 4;
		else if (x >= 3 && x < 6 && y >= 6)
			return 5;

		else if (x >= 6 && y < 3)
			return 6;
		else if (x >= 6 && y >= 3 && y < 6)
			return 7;
		else if (x >= 6 && y >= 6)
			return 8;
		return -1;
	}
}
